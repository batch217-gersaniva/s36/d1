// Setup dependencies/modules

const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");

// Server Setup
const app = express();
const port = 3001;


// Midllewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// DB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.pzjdphc.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true, 
	useUnifiedTopology : true
});

//localhost:3001/tasks/viewTasks
//localhost:3001/tasks/addNewTask

app.use("/tasks", taskRoute);


app.use("/tasks", taskRoute);
app.listen(port, () => console.log(`Now listening to port ${port}`));