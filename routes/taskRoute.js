// It contains all end points for our application

const express = require("express");
const router = express.Router();  //Router() ---> is a module
const taskController =require("../controllers/taskController.js")

//[SECTION] Routes
// It is responsible for deleting or creating endpoints
// All the business logic is done in the controller


router.get("/viewTasks", (req, res) => {
	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});



router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	});
})

router.put("/updateTask/:id", (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// Process a GET request at the "/tasks/specificTask/:id" route using postman to get a specific task.

router.get("/specificTask:id", (req, res) => {
	taskController.getSpecificTask().then(resultFromController => res.send(resultFromController));
})


router.put("/task/:id/complete", (req,res) => {
	taskController.completedTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
